package spark_systems

import (
	"github.com/jinzhu/gorm"
)

func (m SQLModel) FindRelation(db interface{}, column string, model interface{}, relation interface{}) interface{} {

	switch v := db.(type) {
	case *gorm.DB:
		return v.Model(model).Association(column).Find(relation)
	}

	return nil

}
