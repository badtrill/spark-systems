package spark_systems

import (
	"github.com/aofei/air"
	driver "github.com/arangodb/go-driver"
	"github.com/jinzhu/gorm"
)

type SQL struct {
	ORM   *gorm.DB
	Model SQLModel
}

type DB struct {
	SQL   SQL
	Graph driver.Collection
}

type SQLModel struct {
	gorm.Model
}

type Spark struct {
	DB     *DB
	Router *air.Air
}

func NewSpark(Router *air.Air, dbs ...interface{}) *Spark {

	var db *DB

	if len(dbs) != 0 {
		for i := len(dbs) - 1; i >= 0; i-- {
			switch v := dbs[i].(type) {
			case *gorm.DB:
				db = &DB{
					SQL: SQL{
						v,
						SQLModel{},
					},
				}
			case driver.Collection:
				db = &DB{
					Graph: v,
				}
			}
		}
	}

	return &Spark{DB: db, Router: Router}
}
