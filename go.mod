module gitlab.com/badtrill/spark-systems

go 1.12

require (
	github.com/aofei/air v0.14.1
	github.com/arangodb/go-driver v0.0.0-20200107125107-2a2392e62f69
	github.com/jinzhu/gorm v1.9.12
)
